//
// Created by flo on 21/10/2020.
//

#ifndef KINBALL_CLUB_H
#define KINBALL_CLUB_H

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include "../math/point.h"

using namespace std;

static const int SIZE_NAME_CLUB = 16;
static const string LETTERS =
        "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";

// Permet de gérer un club
class Club {
private:
    string name;
    Point* coords;
public:
    // Créé un club avec un nom et des coordonnées aléatoires
    Club();
    // Créé un club avec des coordonnées aléatoires et un nom passé en paramètre
    Club(string name);

    // Retourne le nom du club
    string GetName();
    // Retourne les coordonnées du club
    Point* GetCoords();

    // Change l'attribut de name avec la chaine de caractère passé en paramètre
    void SetName(string name);
    // Change l'attribut de coords avec les coordonnées passées en paramètre
    void SetCoords(int x, int y);

    // Calcul la distance entre deux clubs
    double DistClub(Club* cl);
    // Retourne une version en chaine de caractère de la classe
    string Str();
};

#endif //KINBALL_CLUB_H
