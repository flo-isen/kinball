//
// Created by flo on 21/10/2020.
//

#include "competition.h"

Competition::Competition() {
    Club* club;
    for (int i = 0; i < NUMBER_CLUBS; i++) {
        club = new Club();
        clubs.insert(pair<string, Club*>(club->GetName(), club));
    }
}

void Competition::DisplayClubs() {
    map<string, Club*>::iterator itr;
    for (itr = clubs.begin(); itr != clubs.end(); ++itr) {
        cout << itr->second->Str() << endl;
    }
}

void Competition::DisplayDistances() {
    for (auto itr = clubs.begin(); itr != clubs.end(); ++itr) {
        int dist = 0;
        for (auto itrInt = clubs.begin(); itrInt != clubs.end(); ++itrInt) {
            if (itr->first.compare(itrInt->first) != 0) {
                dist += (int) (itr->second->DistClub(itrInt->second)) * 2;
            }
        }
        cout << itr->first << " : " << dist << " km" << endl;
    }
}

map<string, Club*> Competition::GetClubs() {
    return clubs;
}


