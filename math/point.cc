//
// Created by flo on 21/10/2020.
//

#include "point.h"

Point::Point() {
    x = 0;
    y = 0;
}

Point::Point(int x, int y) : x(x), y(y) {}

int Point::GetX() { return x; }

int Point::GetY() { return y; }

void Point::SetX(int x) { this->x = x; }

void Point::SetY(int y) { this->y = y; }

double Point::Dist(Point* pt) {
    return hypot(x - pt->GetX(), y - pt->GetY());
}

string Point::Str() {
    return to_string(x) + " " + to_string(y);
}
