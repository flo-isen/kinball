# KinBall

## Sommaire

1. Comment lancer le projet
2. Présentation des fonctions
    1. Math
    2. Competition
3. Explicatios des choix
4. Images

## Comment lancer le projet

Afin de lancer le projet veuillez vous rendre dans le repertoire "cmake-build-debug".
Puis lancer les commandes suivantes :
```
make
./kinball
```

## Présentation des fonctions
### Math
#### Point

Constructeurs:
    
    // Créé un point ayant pour coordonnée  (0, 0)
    Point();
    // Créé un point avec les coordonnée passé en paramètre
    Point(int x, int y);
    

Fonctions:
    
    // Retourne la coordonnée en X
    int GetX();
    // Retourne la coordonnée en Y
    int GetY();
        
    // Change l'attribut de x avec l'entier passé en paramètre
    void SetX(int x);
    // Change l'attribut de y avec l'entier passé en paramètre
    void SetY(int y);
        
    // Calcul la distance entre deux points
    double Dist(Point* pt);
       
    // Retourne une version en chaine de caractère de la classe
    string Str();
    

### Competition
#### Club

constructeurs:

    // Créé un club avec un nom et des coordonnées aléatoires
    Club();
    // Créé un club avec des coordonnées aléatoires et un nom passé en paramètre
    Club(string name);

Fonctions:
    
    // Retourne le nom du club
    string GetName();
    // Retourne les coordonnées du club
    Point* GetCoords();

    // Change l'attribut de name avec la chaine de caractère passé en paramètre
    void SetName(string name);
    // Change l'attribut de coords avec les coordonnées passées en paramètre
    void SetCoords(int x, int y);

    // Calcul la distance entre deux clubs
    double DistClub(Club* cl);
    // Retourne une version en chaine de caractère de la classe
    string Str();

#### Competition
constructeur:
    
    // Créé une competition avec 10 club généré aléatoirement
    Competition();
    
fonctions:

    // Retourne la map des clubs
    map<string, Club*> GetClubs();
    // Renvoie dans la console les distances que doit faire chaque club pour
    // aller voir les autres
    void DisplayDistances();
    // Renvoie dans la console les clubs avec leurs coordonnées
    void DisplayClubs();

## Explications des choix
Afin de créér une liste de club, l'utilisation de la map a été utilisé afin de ne pas avoir deux clubs ayant le même
nom. De même, j'ai utilisé une classe Point pour définir les coordonnées d'un club au lieu d'utiliser un vecteur.

## Images
### Sorties du programmes
![Liste des clubs](/images/list_clubs.png "Liste des clubs")
![Liste des km pour chaque clubs](/images/list_km_clubs.png "Liste des km par clubs")

### Fichier Point
![Fichier Point](/images/point.png "Fichier Point")

### Fichier Club
![Fichier Club](/images/club_1.png "Fichier Club")
![Fichier Club](/images/club_2.png "Fichier Club")

### Fichier Competition
![Fichier Competition](/images/competition_1.png "Fichier Competition")
![Fichier Competition](/images/competition_2.png "Fichier Competition")

### Fichier Main
![Fichier main](/images/main.png "Fichier main")

