//
// Created by flo on 21/10/2020.
//

#ifndef KINBALL_POINT_H
#define KINBALL_POINT_H

#include <string>
#include <cmath>

using namespace std;

// Permet de gérer des coordonnées
class Point {
private:
    int x;
    int y;
public:
    // Créé un point ayant pour coordonnée  (0, 0)
    Point();
    // Créé un point avec les coordonnée passé en paramètre
    Point(int x, int y);

    // Retourne la coordonnée en X
    int GetX();
    // Retourne la coordonnée en Y
    int GetY();

    // Change l'attribut de x avec l'entier passé en paramètre
    void SetX(int x);
    // Change l'attribut de y avec l'entier passé en paramètre
    void SetY(int y);

    // Calcul la distance entre deux points
    double Dist(Point* pt);

    // Retourne une version en chaine de caractère de la classe
    string Str();
};


#endif //KINBALL_POINT_H
