//
// Created by flo on 21/10/2020.
//

#ifndef KINBALL_COMPETITION_H
#define KINBALL_COMPETITION_H

#include <iostream>
#include <map>
#include "club.h"

using namespace std;

static int NUMBER_CLUBS = 10;

class Competition {
private:
    map<string, Club*> clubs;
public:
    // Créé une competition avec 10 club généré aléatoirement
    Competition();

    // Retourne la map des clubs
    map<string, Club*> GetClubs();
    // Renvoie dans la console les distances que doit faire chaque club pour
    // aller voir les autres
    void DisplayDistances();
    // Renvoie dans la console les clubs avec leurs coordonnées
    void DisplayClubs();
};


#endif //KINBALL_COMPETITION_H
