//
// Created by flo on 21/10/2020.
//

#include "main.h"

int main(int argc, char** argv) {
    srand(time(NULL));
    Competition* cmp = new Competition();
    cmp->DisplayClubs();
    cmp->DisplayDistances();
    delete cmp;

    return 0;
}
