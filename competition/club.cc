//
// Created by flo on 21/10/2020.
//

#include "club.h"

Club::Club() {
    name = "";
    for (int j = 0; j < rand() % SIZE_NAME_CLUB + 5; j++) {
        name += LETTERS[rand() % LETTERS.size() - 1];
    }
    this->coords = new Point(rand() % 100, rand() % 100);
}

Club::Club(string name) : name(name) {
    this->coords = new Point(rand() % 100, rand() % 100);
}

string Club::GetName() {
    return name;
}

void Club::SetName(string name) { this->name = name; }

Point* Club::GetCoords() { return coords; }

void Club::SetCoords(int x, int y) {
    coords->SetX(x);
    coords->SetY(y);
}

double Club::DistClub(Club* cl) {
    return this->coords->Dist(cl->GetCoords());
}

string Club::Str() {
    return this->GetName() + " : [ " + this->GetCoords()->Str() + " ]";
}
